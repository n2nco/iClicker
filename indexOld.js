
// const dotenv = require('dotenv')
const chromium = require('chrome-aws-lambda');

exports.handler = async (event, context, callback) => {
  console.log('running handler - printing in order: event, context, callback:');
  console.log(event)
  console.log(context)
  console.log(callback);
  
  let result = null;
  let browser = null;

  //  callback ? callback = callback : callback = (text) => console.log(text)

  try {
    console.log('trying to launch chromium.puppeteer')
    browser = await chromium.puppeteer.launch({
      args: chromium.args,
      defaultViewport: chromium.defaultViewport,
      executablePath: await chromium.executablePath,
      headless: chromium.headless, //interesting
      ignoreHTTPSErrors: true,
    });

    const page = await browser.newPage()
    await page.goto('https://student.iclicker.com/#/login')
    console.log('completed goto')

    await page.setViewport({ width: 1703, height: 994 })
   

    await page.waitForSelector('#userEmail')
    await page.click('#userEmail')
    console.log(`completedclick('#userEmail')`)

    await page.type('#userEmail', 'palak.barman@ubc.ca')
    await page.type('#userPassword', 'Ubciclicker1')

    await page.waitForSelector('#sign-in-button')
    await page.click('#sign-in-button')
    console.log('clicked sign in')
    await page.waitForSelector('div > .selectable-item-list-courses:nth-child(3) > .ng-scope > a > .course-title')
   

    //works
    await page.evaluate( async () => {
        let x = document.getElementsByClassName("course-title")
        let courses = Array.from(x)
        console.log('got courses: ' + courses)

        x[courses.findIndex( (item) => { return item.innerHTML.includes('CPSC 101') })].click()
    })
    console.log('clicked CPSC 101')
    // await page.waitForNavigation()
  

   await page.evaluate(async () => {
      setTimeout(() => {
        console.log('running settimeout func')
        if(!document.title.includes('Class activity details') && (!document.title.includes('Polling'))) {
          console.log('clicking join btn')
            document.getElementById('btnJoin')?.click()
        }
    }, 6000)

    })
    // await page.waitForNavigation()
    console.log('about to run question answering setInterval')

    // let wasAnswerSelected = false


    
    await page.waitForFunction(() => document.readyState === "complete");

    await page.waitForSelector("#polling", {timeout: 0}) //TIMEOUT DISABDLED FOR NOW
    let pollingPageTitle = await page.title()
    console.log('pollingPageTitle:')
    console.log(pollingPageTitle)

    await page.waitForSelector('#multiple-choice-a, #multiple-choice-b, #multiple-choice-c, #multiple-choice-d')
    console.log(' found one of #multiple-choice-a, #multiple-choice-b, #multiple-choice-c, #multiple-choice-d')
   
    //     console.log('running button answer click evaluate')
    // let polling = false
    // if (pollingPageTitle.includes('Polling')) {
    //   while (polling) {
    //   console.log('polling while loop')

    //1st selector is quiz, 2nd is class ended, 3rd is 'you're checked in' without quiz started
  
   //while 
   let pageWereOn = await page.waitForSelector('.question-type-banner', "course-screen", ".unified-check-in-icon", {timeout: 0})
   

    
    await page.waitForTimeout(3000);

   
   console.log('found the following selector from selection:')
    console.log(xx)

   let wasClicked = await page.evaluate(async () => {
        try {
          let letters = ["a", "b", "c", "d"];
          let randomLetter = letters[Math.floor(Math.random()*letters.length)];
          console.log('randomLetter: ' + randomLetter)
          console.log("document.title-p e")
          console.log(document.title)

          let answerReceived = document.getElementById('status-text-container-id')?.innerHTML?.includes('Answer Received')
          let questionConcluded = document.getElementsByClassName('answer-label')[0]?.innerHTML?.includes('Your Answer:')
          if(answerReceived) {console.log('answer for this q already received')}
          if(questionConcluded) {console.log('question concluded')}
          if (!answerReceived && !questionConcluded) {
            console.log('answer for this q not already received')
            let btnElement = document.getElementById(`multiple-choice-${randomLetter}`);
            if (btnElement) {
                btnElement.click();
                console.log(`multiple-choice-${randomLetter} clicked`)
                return true
                // wasAnswerSelected = true
            }
            else { //if a quiz pops up // might want to re-work this as now it's dependenft on child elements structure
              let quizMcqBtn = document.querySelector("#quizzing-question > div:nth-child(5) > div > div:nth-child(1) > button")
              if (quizMcqBtn) { 
                console.log('quizMcqAnswerFound. Clicking now.') 
                quizMcqBtn.click()}
            }
        }
      }
      catch(e) {
          console.log('error caught - no id of multuple-choice-a in questionPage.js')
          console.log(e)
          return false
      }
    })
   let x = await wasClicked
   console.log('was a MCQ answer clicked?:')
   console.log(x)
    //   })
    // }

   

    // await Promise.all([
    //   page.click('button[type=submit]'),
    //   page.waitForNavigation({waitUntil: 'networkidle2'})
    // ]);



    


    // let quizOngoing = true
    // while(quizOngoing) {
    //   await page.evaluate(
    //     setTimeout (() => {
    //       //   setInterval( async () => {
    //           console.log('running setInterval button click')
    //             // if (!wasAnswerSelected) {
    //             try {
    //                 let randomLetter = letters[Math.floor(Math.random()*letters.length)];
    //                 console.log('randomLetter: ' + randomLetter)
        
    //                 let btnElement = document.getElementById(`multiple-choice-${randomLetter}`);
    //                 if (btnElement) {
    //                     btnElement.click();
    //                     console.log(`multiple-choice-${randomLetter} clicked`)
    //                     // wasAnswerSelected = true
    //                 }
    //             }
    //             catch(e) {
    //                 console.log('error caught - no id of multuple-choice-a in questionPage.js')
    //                 console.log(e)
    //             }
              
    //         // }
        
    //     //     }, 5000)
    //      }, 4000)
    // }
 
  //   await page.evaluate(async () => {
  //     // let pt3 = await page.title()
  //     console.log("document.title line 82")
  //     console.log(document.title)

  //     let letters = ["a", "b", "c", "d"];
  //     console.log('running button answer click evaluate')
  //     return new Promise(setTimeout (() => {
  //     //   setInterval( async () => {
  //         console.log('running setInterval button click')
  //           // if (!wasAnswerSelected) {
  //           try {
  //               let randomLetter = letters[Math.floor(Math.random()*letters.length)];
  //               console.log('randomLetter: ' + randomLetter)
    
  //               let btnElement = document.getElementById(`multiple-choice-${randomLetter}`);
  //               if (btnElement) {
  //                   btnElement.click();
  //                   console.log(`multiple-choice-${randomLetter} clicked`)
  //                   // wasAnswerSelected = true
  //               }
  //           }
  //           catch(e) {
  //               console.log('error caught - no id of multuple-choice-a in questionPage.js')
  //               console.log(e)
  //           }
          
  //       // }
    
  //   //     }, 5000)
  //    }, 4000))
  // })


    // if (document.title.includes("iClicker Student - Courses")) {
      // console.log('iClicker Student - Courses')
    
      // let x = document.getElementsByClassName("course-title")
      // let courses = Array.from(x)
      // console.log('got courses: ' + courses)
    
      // x[courses.findIndex( (item) => { return item.innerHTML.includes('CPSC 101') })].click()
 

      // await page.waitForSelector('#wrapper')
      // await page.click('#wrapper')

    let pt = await page.title()
    let ev = event

    const response = {
      statusCode: 200,
      body: JSON.stringify(`response from index.js: Event: ${event}! + Page title: ${pt}`),
    };
    console.log('ending try block')
    return response;
  //  globalEventVar = event
  //  globalPageTitle = await page.title()
  //  console.log('pageTitle: ' + pageTitle)
}



 catch (error) {
    console.log('inside catch - error in test.js:')
    console.log(error)
    return error;
 } 
 finally {
  console.log('inside finally - setTimeout will be closing browser')

    setTimeout(async () => {
      // const response = {
      //   statusCode: 200,
      //   body: JSON.stringify(`response from index.js: Event: ${globalEventVar}! + Page title: ${globalPageTitle}`),
      // };
        if (browser !== null) {
            console.log('closing browser')
          await browser.close();
        }
      }
    , 15000);
  }
}
    


// const start = async () => {

//     console.log('entering')

    

//     const puppeteer = require('puppeteer');
//     const browser = await puppeteer.launch({headless: false})

//     const page = await browser.newPage()
//     await page.goto('https://student.iclicker.com/#/login')
//     console.log('completed goto')

//     await page.setViewport({ width: 1703, height: 994 })

//     await page.waitForSelector('#userEmail')
//     await page.click('#userEmail')

//     await page.type('#userEmail', 'palak.barman@ubc.ca')
//     await page.type('#userPassword', 'Ubciclicker1')

//     await page.waitForSelector('#sign-in-button')
//     await page.click('#sign-in-button')

//     await page.waitForSelector('div > .selectable-item-list-courses:nth-child(3) > .ng-scope > a > .course-title')
   
//     await page.evaluate( async () => {
//         let x = document.getElementsByClassName("course-title")
//         let courses = Array.from(x)
//         console.log('got courses: ' + courses)

//         x[courses.findIndex( (item) => { return item.innerHTML.includes('CPSC 100') })].click()
//     })

//     await page.waitForSelector('#wrapper')
//     await page.click('#wrapper')

// }
// start() 

