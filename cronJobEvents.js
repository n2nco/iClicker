// Load the AWS SDK for Node.js
var AWS = require('aws-sdk');

var credentials = new AWS.SharedIniFileCredentials({profile: 'default'});
AWS.config.credentials = credentials;

// var cwevents = new AWS.CloudWatchEvents({apiVersion: '2015-10-07'});
// var params = {
//     Name: 'DEMO_EVENT',
//     RoleArn: 'arn:aws:iam::518753408065:policy/CloudWatchEventsFullAccessCustomJSON', //https://us-east-1.console.aws.amazon.com/iam/home#/policies/arn:aws:iam::518753408065:policy/CloudWatchEventsFullAccessCustomJSON$jsonEditor
//     ScheduleExpression: 'rate(5 minutes)',
//     State: 'ENABLED'
//     };
    
//     cwevents.putRule(params, function(err, data) {
//     if (err) {
//         console.log("Error", err);
//     } else {
//         console.log("Success", data.RuleArn);
//     }
//     });
    

AWS.config.update({region: 'us-west-2'});

// Create CloudWatchEvents service object
var cwevents = new AWS.CloudWatchEvents({apiVersion: '2015-10-07'});


var params = {
  Rule: 'DEMO_EVENT',
  Targets: [
    {
      Arn: 'arn:aws:lambda:us-west-2:518753408065:function:iclickedFunc',
      Id: 'myCloudWatchEventsTarget',
    }
  ]
};

cwevents.putTargets(params, function(err, data) {
  if (err) {
    console.log("Error", err);
  } else {
    console.log("Success putting target to event", data);
  }
});