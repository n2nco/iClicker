// Load the AWS SDK for Node.js
var AWS = require('aws-sdk');
// Set the region 
AWS.config.update({region: 'us-west-2'});

// Create CloudWatchEvents service object
var cwevents = new AWS.CloudWatchEvents({apiVersion: '2015-10-07'});

var params = {
  Name: 'DEMO_EVENT3',
  RoleArn: 'arn:aws:iam::518753408065:role/CloudWatchFullAccess',
  ScheduleExpression: 'rate(1 minute)',
  State: 'ENABLED',
  //2 fields new update after running - aws events describe-rule --name "DEMO_EVENT3"
  EventBusName: "default",
  CreatedBy: "518753408065"
};


cwevents.putRule(params, function(err, data) {
  if (err) {
    console.log("Error", err);
  } else {
    console.log("Success running rule 'DEMO_EVENT2", data.RuleArn);
  }
});